# vue入门教程

## 概览

### vue是什么？

> Vue.js（/vjuː/，或简称为Vue）是一个用于创建用户界面的开源JavaScript框架，也是一个创建单页应用的Web应用框架。

> 响应式的数据绑定,相较于jquery之类的类库，vue能够帮助我们减少不必要的dom操作，提高渲染的效率；我们只需要关心数据的业务逻辑，不需要再去关心dom如何去渲染了。

> 可组合的视图组件：把视图按照功能切分成若干基本单元，组件可以一级一级组合整个应用形成倒置组件树，可维护，可重用，可测试。

## 准备

### 前置知识

#### html\css\js

我们假定你已经对 HTML 和 JavaScript 都比较熟悉了。即便你之前使用其他编程语言，你也可以跟上这篇教程的。除此之外，我们假定你也已经熟悉了一些编程的概念，例如，函数、对象、数组，以及 class 的一些内容。

如果你想回顾一下 JavaScript，你可以阅读这篇教程。注意，我们也用到了一些 ES6（较新的 JavaScript 版本）的特性。在这篇教程里，我们主要使用了箭头函数（arrow functions）、class、let 语句和 const 语句。

#### nodejs

> Node.js 是一个开源与跨平台的 JavaScript 运行时环境。 它是一个可用于几乎任何项目的流行工具！
Node.js 在浏览器外运行 V8 JavaScript 引擎（Google Chrome 的内核）。类似于java的JDK；

> nodejs内部内置包管理工具，类似于java的maven，是管理依赖包的工具；

> [nodejs入门教程](http://nodejs.cn/learn)

> 并不需要太过深入了解，只需要知道如何去安装该nodejs环境即可。

#### npm

> npm 是 Node.js 标准的软件包管理器。

> 它起初是作为下载和管理 Node.js 包依赖的方式，但其现在也已成为前端 JavaScript 中使用的工具。

以下是比较常用的命令：

---

安装所有依赖

```bash
npm install
```

它会在 node_modules 文件夹（如果尚不存在则会创建）中安装项目所需的所有东西。

***

安装单个软件包

也可以通过运行以下命令安装特定的软件包：

```bash
npm install <package-name>
```

通常会在此命令中看到更多标志：

- --save 安装并添加条目到 package.json 文件的 dependencies。
- --save-dev 安装并添加条目到 package.json 文件的 devDependencies。
区别主要是，devDependencies 通常是开发的工具（例如测试的库），而 dependencies 则是与生产环境中的应用程序相关。

---

当使用 npm 安装软件包时，可以执行两种安装类型：

- 本地安装
- 全局安装

默认情况下，当输入 npm install 命令时，例如：

```bash
npm install lodash
```


软件包会被安装到当前文件树中的 node_modules 子文件夹下。

在这种情况下，npm 还会在当前文件夹中存在的 package.json 文件的 dependencies 属性中添加 lodash 条目。

使用 -g 标志可以执行全局安装：

```bash
npm install -g lodash
```

在这种情况下，npm 不会将软件包安装到本地文件夹下，而是使用全局的位置。

---

package.json 文件支持一种用于指定命令行任务（可通过使用以下方式运行）的格式：

```bash
npm run <task-name>
```

例如：

```json
{
  "scripts": {
    "start-dev": "node lib/server-development",
    "start": "node lib/server-production"
  },
}
```

因此可以运行如下，而不是输入那些容易忘记或输入错误的长命令：

```bash
$ npm run watch
$ npm run dev
$ npm run prod
```







### 环境准备

#### 安装nodejs

[下载安装链接](http://nodejs.cn/download/)

> 建议选择最新的msi文件进行下载安装，会配置好一些环境变量

> 安装完成之后既可以执行以下命令
>
> ```bash
> C:\Users\90744> node -v
> v14.17.0
> ```
>
> ```bash
> C:\Users\90744> npm -v
> 6.14.13
> ```
>
> 以上命令分别输出nodejs以及npm的版本。

> 注意：npm安装包时默认使用的是官方源，比较慢，建议更换国内的源：
>
> ```bash
> npm config set registry https://registry.npm.taobao.org
> ```
>
> 执行以上命令，就会将npm的源切换到国内的淘宝源。

#### 安装vue cli

ue CLI 是一个基于 Vue.js 进行快速开发的完整系统，提供：

- 通过 `@vue/cli` 实现的交互式的项目脚手架。

- 通过 `@vue/cli` + `@vue/cli-service-global` 实现的零配置原型开发。

- 一个运行时依赖 (

  ```
  @vue/cli-service
  ```

  )，该依赖：

  - 可升级；
  - 基于 webpack 构建，并带有合理的默认配置；
  - 可以通过项目内的配置文件进行配置；
  - 可以通过插件进行扩展。

- 一个丰富的官方插件集合，集成了前端生态中最好的工具。

- 一套完全图形化的创建和管理 Vue.js 项目的用户界面。

> Node 版本要求
>
> Vue CLI 4.x 需要 [Node.js](https://nodejs.org/) v8.9 或更高版本 (推荐 v10 以上)。你可以使用 [n](https://github.com/tj/n)，[nvm](https://github.com/creationix/nvm) 或 [nvm-windows](https://github.com/coreybutler/nvm-windows) 在同一台电脑中管理多个 Node 版本。

> 可以使用下列任一命令安装这个新的包：
>
> ```bash
> npm install -g @vue/cli
> ```
>
> 安装之后，你就可以在命令行中访问 `vue` 命令。你可以通过简单运行 `vue`，看看是否展示出了一份所有可用命令的帮助信息，来验证它是否安装成功。
>
> 你还可以用这个命令来检查其版本是否正确：
>
> ```bash
> vue --version
> ```

### 概览



#### 我们会做出什么东西？

我们将在这个教程中开发一个计划清单的工具。你将在这个教程中学到关于构建Vue应用的基础知识，掌握这些知识后，你将会对Vue有更加深刻的理解。

你可以提前预览我们要写的应用的**[最终效果](https://todos.qipo.net/)**。如果你看不懂其中的代码，或不熟悉这些语法，别担心！接下来的教程会一步一步帮助你理解 Vue 及其语法。

你可以提前看到项目的最终代码 [vue-todo-demo: 一个用vue做的计划清单的演示项目 (gitee.com)](https://gitee.com/keyarea/vue-todo-demo)

#### 创建一个vue项目

之前我们全局安装了vue cli，我们现在可以用命令行生成一个项目：

```bash
vue create todos
```

![](https://assets.qipo.net/create-1.PNG)

![](https://assets.qipo.net/create-2.PNG)

![](https://assets.qipo.net/create-3.PNG)

然后根据提示，进入到项目目录下

```bash
cd todos
```

然后运行项目即可

```bash
npm run serve
```

![](https://assets.qipo.net/run-vue.PNG)

然后我们就可以在浏览器中打开上述地址`http://localhost:8080/`查看项目

![](https://assets.qipo.net/vue-web.PNG)

#### 安装需要用到的依赖

##### 状态管理依赖

> Vuex 是一个专为 Vue.js 应用程序开发的**状态管理模式**。它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化。

安装，在项目目录下执行

```bash
npm install vuex --save
```

##### 路由管理器

Vue Router 是 [Vue.js (opens new window)](http://cn.vuejs.org/)官方的路由管理器。它和 Vue.js 的核心深度集成，让构建单页面应用变得易如反掌。包含的功能有：

- 嵌套的路由/视图表
- 模块化的、基于组件的路由配置
- 路由参数、查询、通配符
- 基于 Vue.js 过渡系统的视图过渡效果
- 细粒度的导航控制
- 带有自动激活的 CSS class 的链接
- HTML5 历史模式或 hash 模式，在 IE9 中自动降级
- 自定义的滚动条行为

安装，在项目目录下执行

```bash
npm install vue-router --save
```

##### Element

> 一套为开发者、设计师和产品经理准备的基于 Vue 2.0 的桌面端组件库，[项目地址](https://element.eleme.cn/#/zh-CN)

安装，在项目目录下执行

```bash
npm i element-ui -S
```

#### 在项目中引入需要用到的依赖

##### vuex

###### 项目结构

Vuex 并不限制你的代码结构。

对于大型应用，我们会希望把 Vuex 相关代码分割到模块中。下面是项目结构示例：

```
├── index.html
├── main.js
├── api
│   └── ... # 抽取出API请求
├── components
│   ├── App.vue
│   └── ...
└── store
    ├── index.js          # 我们组装模块并导出 store 的地方
    └── modules
        ├── app.js       # app的全局状态
        └── todos.js   # 待办事项
        └── lists.js   # 待办事项分类
```

index.js: 我们组装模块并导出 store 的地方

```vue
import Vue from 'vue'
import Vuex from 'vuex'
import { createLogger } from 'vuex'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: { 
    // 分别引用modules目录下的文件，后面会有讲到
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
```

然后我们需要在`main.js`中将该文件引入

```js
import Vue from 'vue'
// 这个将会是我们的第一层页面，即为首页
import App from './App.vue'
// 这个将会导入我们刚才写的store目录下的index.js文件
import store from './store'

Vue.config.productionTip = false

new Vue({
  // 然后我们需要在vue实例化时引入
  store,
  render: h => h(App)
}).$mount('#app')
```

这样我们的状态管理模块vuex就初步引入了。

##### vue-router

项目结构

```
├── index.html
├── main.js
├── App.vue
├── api
│   └── ... # 抽取出API请求
├── views
│   └── Home.vue
└── router
    └── index.js
```

我们需要将`App.vue`改写，以用于vue-router进行路由管理：

```vue
<template>
  <div id="app">
    <router-view></router-view>
  </div>
</template>

<style>
body, html {
  width: 100%;
  height: 100%;
  margin: 0;
}
#app {
  width: 100%;
  height: 100%;
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: rgba(0,0,0,.65);
  background-color: #001529;
}
</style>
```

> 从上面可以看到，我们将`<div id="app"></div>`中的元素进行了删除，并添加了`<router-view></router-view>`，该元素将会是路由出口，路由匹配到的组件将会被渲染在这里，可能我们对这里的概念比较懵，让我们继续看下去。

然后我们在`views`目录下创建一个新的组件`Home.vue`

```vue
<template>
  <div class="home">
      {{msg}}
  </div>
</template>

<script>
export default {
  name: 'Home',
  data() {
      return {
          msg: 'HelloWorld'
      }
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>
```

> 一个组件分成了三个部分，分别是`<template></template>`、`<script></script>`、`<style></style>`
>
> - `<template></template>`: 界面展示代码
> - `<script></script>`: 业务实现代码
> - `<style></style>`: 界面布局代码
>
> 在以上的vue组件中，我们使用了`{{}}`，数据绑定最常见的形式就是使用“Mustache”语法 (双大括号) 的文本插值，Mustache 标签将会被替代为对应数据对象上 `msg` property 的值。无论何时，绑定的数据对象上 `msg` property 发生了改变，插值处的内容都会更新。
>
> 可想而知，我们`{{}}`中引用的变量是`script`中由`data`函数导出的变量，组件的data只能是一个函数，这是Vue 实例的数据对象。我们组件中的变量一般保存在这里，我们可以在组件的其他地方使用`this.msg = 'nihao'`对该值进行重新赋值，这更改会体现在界面上。

我们会在`router`目录下创建`index.js`中管理vue的路由。

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
// 引用组件 @代表着项目的根目录
import Home from '@/views/Home'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
```

然后我们必须在`main.js`中引用该文件。

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
```

最终我们看到的界面将会是：

![](https://assets.qipo.net/vue-helloworld.PNG)

> 以上我们可以清晰的看到，`router-view`被`Home`组件替代了，而我们的路由正是`/`，在该路由下我们引用了Home.vue，这就是我们之前说
>
> *该元素将会是路由出口，路由匹配到的组件将会被渲染在这里*
>
> 的原因。
>
> 也看到了我们data中我们msg的变量被成功渲染为了`HelloWorld`的值。

##### ElementUI

我们只需要在`main.js`中引入该模块即可

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入了Element 
import ElementUI from 'element-ui';
// 样式文件需要单独引入
import 'element-ui/lib/theme-chalk/index.css';

import moment from 'moment/moment'
// 使用该模块
Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
```

借助 [babel-plugin-component](https://github.com/QingWei-Li/babel-plugin-component)，我们可以只引入需要的组件，以达到减小项目体积的目的。

首先，安装 babel-plugin-component：

```bash
npm install babel-plugin-component -D
```

我们还需要在`babel.config.js`中配置以下

```js
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ]
}
```

> 以上我们就引入了`ElementUI`

我们开始使用该UI吧。

```vue
<template>
  <div class="home">
      {{msg}}
      <el-button type="primary">按钮</el-button>
  </div>
</template>

<script>
export default {
  name: 'Home',
  data() {
      return {
          msg: 'HelloWorld'
      }
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>
```

然后我们启动项目就可以看到：

![](https://assets.qipo.net/vue-element-btn.PNG)

如上所见，我们成功的引用了ElementUI模块。

#### 明确项目结构

```
├── public
│   ├── index.html
│   └── favicon.ico
├── src
│   ├── App.vue
│   ├── main.js
│   ├── assets // 该文件夹包含了我们用到的静态资源
│   ├── common // 该文件夹包含了我们用到的通用的方法
│   ├── utils  // 该文件夹包含了我们用到的通用的工具方法
│   ├── components // 该文件夹包含了我们需要用到的组件
│   ├── domain // 该文件夹包含了我们用到的一些类，我们直接可以通过这些类创建我们需要的对象
│   ├── router //  该文件夹包含了我们用到的路由管理
│   ├── store // 该文件夹包含了我们在全局用到的状态变量
│   ├── views // 该文件夹包含了我们路由导航的页面
│   └── api // 该文件夹我们暂时用不到，这里应该放置所有的网络请求，抽取出API请求，方便管理

```

也就是说，src目录下才是我们编程的主要文件夹。

#### 构思

##### 存储问题

想一下，如果我们做一个计划清单，首要考虑的是存储问题，不能一刷新页面就丢失所有的计划清单。当然在这里最好的方法自然是有个后端服务进行存储，但是如果我们没有后端服务呢？

> 我们可以使用H5新增的`localStorage`进行本地存储，`localStorage`是用户不主动删除永久存在的，当然它根据域名进行区分，以key-value的形式进行存储，在开发者工具中我们可以看到：
>
> ![](https://assets.qipo.net/localstorage.PNG)
>
> 操作的方式一般常用的有以下几种：
>
> - 写入:
>   	localStorage.setItem('key','value');
> - 读取:
>   	localStorage.getItem('key');
> - 删除:
>   	localStorage.removeItem('key');
>
> 基于以上方法，我们可以进行存储的操作，刷新之后也不会丢失数据了。

但是我们想一下，变量的命名我们需要一些常量来约束，防止变量命名混乱。于是我们新建一个`local-storage-namespace.js`由于是公共的，我们可以将其创建在`common`目录下。

```js
// 存储在local-storage的变量的命名
export const GLOBAL_NAMESPACE = 'todo.';

export const APP_INFO_NAMESPACE = GLOBAL_NAMESPACE + 'appInfo.';
export const INIT_FLAG = APP_INFO_NAMESPACE + 'initFlag';
export const START_USING_DATE = APP_INFO_NAMESPACE + 'startUsingDate';

export const USER_INFO_NAMESPACE = GLOBAL_NAMESPACE + 'userInfo.';
export const USERNAME = USER_INFO_NAMESPACE + 'username';
export const AVATAR_CODE = USER_INFO_NAMESPACE + 'avatarCode';

export const TODO_NAMESPACE = GLOBAL_NAMESPACE + 'todo.';
export const TODOS = TODO_NAMESPACE + 'todos';

export const LIST_NAMESPACE = GLOBAL_NAMESPACE + 'list.';
export const LISTS = LIST_NAMESPACE + 'lists';

export const SUMMARY_NAMESPACE = GLOBAL_NAMESPACE + 'summary.';
export const LAST_SUMMARY_DATE = SUMMARY_NAMESPACE + 'lastSummaryDate';
export const SUMMARIES = SUMMARY_NAMESPACE + 'summaries';
```

> 以上就是我们关于存储在localStorage中的变量的命名。整齐划一，我们用到的时候就可以这样：
>
> ```js
> // 引入常量
> import {INIT_FLAG, USERNAME, START_USING_DATE} from '@/common/local-storage-namespace'
> 
> localStorage.setItem(USERNAME,'王学凯');
> ```
>
> 是不是比较规整了一些。

但是我们需要注意一个问题，就是在`localStorage`中存储的全是字符串，所以我们需要一个工具，在存储之前，将对象数组之类的变量变为json，在读取之后，将读取到的json，转为相应的对象数组，所以我们新建一个`local-storage.js`文件，由于是工具类的，我们将他放到`utils`目录下。

```js
// 向储存服务storage中写入数据
export const write = function(key, value) {
    if (!key) {
      return;
    }
    if (value) {
      value = JSON.stringify(value);
    }
    localStorage.setItem(key, value);
}

  // 从储存服务storage中读取主句
export const read = function(key) {
    if (!key) {
      return null;
    }
    const value = localStorage.getItem(key);
    if (value && value !== 'undefined' && value !== 'null') {
      return JSON.parse(value);
    }
    return null;
}

  // 根据key删除对应storage中的数据
export const  remove = function(key) {
    if (!key) {
      return;
    }
    localStorage.removeItem(key);
}
  // 清空storage
export const  clear = function() {
    localStorage.clear();
}
```

> 我们不直接操作`localStorage`，而是间接通过该文件中的方法进行操作`localStorage`

之后我们的操作就变成了

```js
// 引入常量
import {INIT_FLAG, USERNAME, START_USING_DATE} from '@/common/local-storage-namespace'
import {write, read} from '@/utils/local-storage';

write(USERNAME,'王学凯');
```

##### 数据结构问题

>  我们在设计一个程序时,首先要考虑的就是需要实现怎么样的功能,以及实现这些功能需要怎么样的数据结构?

我是这样实现的:

```js
import { generateUUID } from '@/utils/uuid';

export class Todo {
  // 唯一标识
  _id;
  // 计划标题
  title;
  // 创建时间
  createdAt;
  // 计划所属的清单
  listUUID;
  // 计划备注
  desc;
  // 计划是否完成
  completedFlag;
  // 计划完成时间
  completedAt;
  // 截至时间
  dueAt;
  // 计划时间
  planAt;
  // 是否提醒我
  notifyMe;

  constructor({title, listUUID}) {
    this._id = generateUUID();
    this.title = title;
    this.listUUID = listUUID;
    this.completedFlag = false;
  }
}

export class List {
  // 唯一标识
  _id;
  // 清单的标题
  title;
  // 清单创建时间
  createdAt;

  constructor(title) {
    this._id = generateUUID();
    this.title = title;
  }
}
```

> Todo代表的是每一个计划,分别拥有 以上字段字段; List代表清单,Todo与List的关系为多对一

我们可以将以上的文件放置与`domain`目录下,可以方便快捷的引用该文件,并创建计划:

```js
import {Todo} from '@/domain/entities'

const todo = new Todo({title, listUUID}); // 这就会快速创建一个计划
```

##### 页面规划

> 还需要我们合理的规划好页面以及组件.
>
> 页面我们规划的有 初始化页面,首页,设置页面,待办详情页面 四个页面,非常简单的功能.
>
> 组件我们规划的主要是首页上的内容: 左侧区域组件, 右侧区域组件,头部组件,待办列表组件,添加待办组件 一共5个组件.

接下来我们就开始

### 开始

我们首先做一个初始化页面,以供我们获取用户的初始化数据,比如我们需要用户提供给我们一个用户名,然后我们需要将是否初始化以及用户输入的用户名保存到本地存储中.以后我们每次打开该单页面应用需要先读取本地存储中的数据以确定用户数据.



我们新建一个名为SetUp的vue文件.

```vue
<!-- template： html部分 -->
<template>
  <div class="full-screen page-content">
      <div class="wrapper">
          <img class="logo-img" src="../assets/imgs/logo.svg" alt="">
          <div class="text-wrapper">
              <h1 class="title-text">计划清单</h1>
          </div>
          <el-input placeholder="请输入您喜欢的用户名" autofocus v-model="nickname" @input="inputNickname"></el-input>
          <el-button type="primary" :disabled="btnDisabled" @click="completeSetup">开始</el-button>
          <div class="copy-right">Copyright © 2018 keyarea</div>
      </div>
  </div>
</template>

<!-- style: 页面的css样式部分 -->
<style>
.full-screen {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}
.page-content {
    display: flex;
    justify-content: center;
    align-items: center;
    padding-top: 50px;
}
.page-content .wrapper {
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 400px;
    max-height: 420px;
    height: 60vh;
    min-width: 300px;
    width: 30vw;
    max-width: 400px;
    padding: 40px 30px 10px;
    border-radius: 8px;
    background-color: #fff;
}
.page-content .wrapper .logo-img {
    flex: 0 0 120px;
    width: 120px;
    height: 120px;
}
.page-content .wrapper .text-wrapper {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    text-align: center;
}
.page-content .wrapper .text-wrapper .title-text {
    font-size: 24px;
    font-style: italic;
    color: rgba(0,0,0,.65);
}
.page-content .wrapper button {
    margin-top: 26px;
    width: 100%;
}
.page-content .wrapper .copy-right {
    margin-top: 30px;
    flex: 0 0 40px;
}
</style>

<!-- script： 页面的js部分 -->
<script>
import {Logger} from '@/common/logger';
// lodash模块是一个 JavaScript 实用工具库，简化了很多操作。
import * as _ from 'lodash';
import { getTodayTime } from '@/utils/time';

// @ is an alias to /src
const logger = new Logger('SetUp');

export default {
  // 组件的名称
  name: 'SetUp',
  // 需要引入的组件，由于elementUI我们采用的是全局引入，并不需要单独引入
  components: {
  },
  // 组件的生命周期之一，组件的生命周期分别为beforeCreate，created，beforeMount，mounted，beforeUpdate，updated，activated，deactivated，beforeDestroy，destroyed，errorCaptured。这些大部分不会经常使用，只需要记住created，mounted，destroyed，这三个就可以，其他的了解即可，vue官网搜索生命周期钩子。
  created() {
      logger.debug('created');
  },
  // 这是该页面需要的数据对象。组件只接受function，也就是函数。Vue 会递归地把 data 的 property 转换为 getter/setter，从而让 data 的 property 能够响应数据变化。
  data() {
      return {
        // 用户选择的用户名称
          nickname: '',
      }
  },
  // 组件中的方法，我们可以直接通过this访问这些方法。methods中的方法不能使用箭头函数。箭头函数绑定了父级作用域的上下文，所以this将不会按照期望指向Vue实例。
  methods: {
      // 在 Input 值改变时触发
      inputNickname(value) {
          if (_.isEmpty(_.trim(value))) {
              this.btnDisabled = true;
          } else {
              this.btnDisabled = false;
          }
      },
      // 点击开始
      completeSetup() {
          // 修改用户名
          this.$store.dispatch('app/setUserName', this.nickname);
          // 修改用户初始化状态
          this.$store.dispatch('app/setInitFlag', true);
          // 修改用户开始的时间
          this.$store.dispatch('app/setStartUsingDate', getTodayTime());
          // 跳转到主页面
          this.$router.replace({name: 'Home'});
      }
  }
  // 当然在vue实例中还有其他的内容，后续还会介绍几个。都是比较常用的。
}
</script>
```

上面的vue文件明确分为了三个部分,上面我们有明确讲过这三个部分分别代表什么.现在我们的侧重点在于`<template></template>`以及`<script></script>`.

在以上的`<template></template>`中我们看到了我们在`html`中没有见到的

```js
<el-input placeholder="请输入您喜欢的用户名" autofocus v-model="nickname" @input="inputNickname"></el-input>
<el-button type="primary" :disabled="btnDisabled" @click="completeSetup">开始</el-button>
```

> 以上分别引用了`element`中的`input,button`组件,其中有我们没见到的`v-model="nickname"`,`@input="inputNickname"`,`:disabled="btnDisabled"`

#### 向组件中传递参数

##### 传递静态或动态 Prop

像这样，你已经知道了可以像这样给 prop 传入一个静态的值：

```vue
<blog-post title="My journey with Vue"></blog-post>
```

你也知道 prop 可以通过 `v-bind` 动态赋值，例如：

```vue
<!-- 动态赋予一个变量的值 -->
<blog-post v-bind:title="post.title"></blog-post>

<!-- 动态赋予一个复杂表达式的值 -->
<blog-post
  v-bind:title="post.title + ' by ' + post.author.name"
></blog-post>
```

在上述两个示例中，我们传入的值都是字符串类型的，但实际上*任何*类型的值都可以传给一个 prop。

> 例如上面`:disabled="btnDisabled"`我们就是以参数的形式传递到了子组件中.

> 注意: `:title`是`v-bind:title`的简写形式

##### 单向数据流

所有的 prop 都使得其父子 prop 之间形成了一个**单向下行绑定**：父级 prop 的更新会向下流动到子组件中，但是反过来则不行。这样会防止从子组件意外变更父级组件的状态，从而导致你的应用的数据流向难以理解。

额外的，每次父级组件发生变更时，子组件中所有的 prop 都将会刷新为最新的值。这意味着你**不**应该在一个子组件内部改变 prop。如果你这样做了，Vue 会在浏览器的控制台中发出警告。



这里有两种常见的试图变更一个 prop 的情形：

1. **这个 prop 用来传递一个初始值；这个子组件接下来希望将其作为一个本地的 prop 数据来使用。**在这种情况下，最好定义一个本地的 data property 并将这个 prop 用作其初始值：

```vue
props: ['initialCounter'],
data: function () {
  return {
    counter: this.initialCounter
  }
}
```

2. **这个 prop 以一种原始的值传入且需要进行转换。**在这种情况下，最好使用这个 prop 的值来定义一个计算属性：

```vue
props: ['size'],
computed: {
  normalizedSize: function () {
    return this.size.trim().toLowerCase()
  }
}	
```

> 注意   在 JavaScript 中对象和数组是通过引用传入的，所以对于一个数组或对象类型的 prop 来说，在子组件中改变变更这个对象或数组本身**将会**影响到父组件的状态。



#### 监听子组件事件

在`<el-input></el-input>`组件中,肯定需要该组件与父级组件进行沟通.例如我们需要知道该输入框的值,这种是如何做到的呢?

幸好 Vue 实例提供了一个自定义事件的系统来解决这个问题。父级组件可以像处理 native DOM 事件一样通过 `v-on` 监听子组件实例的任意事件,以按钮的点击事件为例:

```vue
// 子组件
<template>
  <button @click="emit('click')">
    按钮
  </button>
</template>
```

其实最底层的还是html那些组件,点击这个button元素,就会触发`emit('click')`,由于父组件监听了click的事件,那么该触发过程就成了:

button触发点击  --> 触发click事件 --> 父组件监听的事件收到 --> 触发completeSetup函数

意义在哪里呢?我们用原生的`<button></button>`不香嘛?

> 意义在于我们可以对native dom button做一些复杂的处理,让我们可以根据组件输入的参数,改变成为不同的按钮形式.比如是否圆角,颜色,图标等等,这样我们之后可以非常简单的复用该组件,只需要改变输入的参数即可.
>
> 当然我们还可以在该自定义组件中完全不使用button native Dom,我们用div等其他的native dom同样可以实现相应的自定义按钮.

> 需要注意的, `@`形式是`v-on`形式的简写,还有相应的修饰符:
>
> ```js
> .stop - 调用 event.stopPropagation()。
> .prevent - 调用 event.preventDefault()。
> .capture - 添加事件侦听器时使用 capture 模式。
> .self - 只当事件是从侦听器绑定的元素本身触发时才触发回调。
> .{keyCode | keyAlias} - 只当事件是从特定键触发时才触发回调。
> .native - 监听组件根元素的原生事件。
> .once - 只触发一次回调。
> .left - (2.2.0) 只当点击鼠标左键时触发。
> .right - (2.2.0) 只当点击鼠标右键时触发。
> .middle - (2.2.0) 只当点击鼠标中键时触发。
> .passive - (2.3.0) 以 { passive: true } 模式添加侦听器
> ```
>
> 示例:
>
> ```html
> <!-- 方法处理器 -->
> <button v-on:click="doThis"></button>
> 
> <!-- 动态事件 (2.6.0+) -->
> <button v-on:[event]="doThis"></button>
> 
> <!-- 内联语句 -->
> <button v-on:click="doThat('hello', $event)"></button>
> 
> <!-- 缩写 -->
> <button @click="doThis"></button>
> 
> <!-- 动态事件缩写 (2.6.0+) -->
> <button @[event]="doThis"></button>
> 
> <!-- 停止冒泡 -->
> <button @click.stop="doThis"></button>
> 
> <!-- 阻止默认行为 -->
> <button @click.prevent="doThis"></button>
> 
> <!-- 阻止默认行为，没有表达式 -->
> <form @submit.prevent></form>
> 
> <!--  串联修饰符 -->
> <button @click.stop.prevent="doThis"></button>
> 
> <!-- 键修饰符，键别名 -->
> <input @keyup.enter="onEnter">
> 
> <!-- 键修饰符，键代码 -->
> <input @keyup.13="onEnter">
> 
> <!-- 点击回调只会触发一次 -->
> <button v-on:click.once="doThis"></button>
> 
> <!-- 对象语法 (2.4.0+) -->
> <button v-on="{ mousedown: doThis, mouseup: doThat }"></button>
> ```
>
> 

##### 使用事件抛出一个值

在事件中,我们也可以使用事件抛出一个值给父组件,`doThat('hello', 'world')`例如这样,就可以将`world`这个字符串传递至父组件.

父组件可以这样去接收:

```vue
// 子组件
<template>
  <div>
    <input @change="emit('hello', 'world')" />
  </div>
</template>
```

假设子组件的名称为 `el-input`

```vue
// 父组件
<template>
	<el-input @hello="hello"></el-input>
</template>
<script>
	export default {
  name: 'SetUp',
  components: {
  },
  created() {
      logger.debug('created');
  },
  data() {
      return {
      }
  },
  methods: {
      hello(value) {
          console.log(value); // world
      },
  }
}
</script>
```



##### 在组件上使用v-model

自定义事件也可以用于创建支持 `v-model` 的自定义输入组件。记住：

```js
<input v-model="searchText">
```

等价于：

```js
<input
  v-bind:value="searchText"
  v-on:input="searchText = $event.target.value"
>
```

自定义组件的`v-model`:

一个组件上的 `v-model` 默认会利用名为 `value` 的 prop 和名为 `input` 的事件，但是像单选框、复选框等类型的输入控件可能会将 `value` attribute 用于[不同的目的](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/checkbox#Value)。`model` 选项可以用来避免这样的冲突：

```vue
Vue.component('base-checkbox', {
  model: {
    prop: 'checked',
    event: 'change'
  },
  props: {
    checked: Boolean
  },
  template: `
    <input
      type="checkbox"
      v-bind:checked="checked"
      v-on:change="$emit('change', $event.target.checked)"
    >
  `
})
```

现在在这个组件上使用 `v-model` 的时候：

```vue
<base-checkbox v-model="lovingVue"></base-checkbox>
```

这里的 `lovingVue` 的值将会传入这个名为 `checked` 的 prop。同时当 `<base-checkbox>` 触发一个 `change` 事件并附带一个新的值的时候，这个 `lovingVue` 的 property 将会被更新。

> 注意你仍然需要在组件的 `props` 选项里声明 `checked` 这个 prop。

#### 变量的共享 

> 非常明显,我们初始化的信息需要全局进行共享,这就离不开我们的`vuex`

> 普及一下js中的异步以及同步，后面会有用：
>
> - 同步：通俗易懂的解释：同步是早上我需要先刷牙，然后再洗脸，有一个先后顺序。
> - 异步：通俗易懂的解释：“异步是早上一边刷牙，同时水壶在烧水，可以同时进行”。比如比较典型的定时器，ajax。
>
> ```js
> function fn(){
>     console.log('start');
>     setTimeout(()=>{
>         console.log('setTimeout');
>     },0);
>     console.log('end');
> }
> 
> fn() // 输出 start end setTimeout
> ```
>
> 

我们需要在`store/module`中创建一个`app.js`其中管理app的一些全局设置

```js
import {INIT_FLAG, USERNAME, START_USING_DATE} from '@/common/local-storage-namespace';
import {write, read} from '@/utils/local-storage';

// 单一状态树
const state = () => ({
    // 用户名
    username: '' || read(USERNAME),
    // 是否已经初始化
    initFlag: false || read(INIT_FLAG),
    // 初次使用时间
    startUsingDate: null || read(START_USING_DATE),
    // 当前选中的菜单
    selectedIndex: '1',
})

// getters getter 的返回值会根据它的依赖被缓存起来，且只有当它的依赖值发生了改变才会被重新计算。
const getters = {
    getInitFlag: state => {
        return state.initFlag;
    },
    // 获取当前选择项是否为列表项
}

// actions 类似于mutations，不同之处在于： 1：Action 提交的是 mutation，而不是直接变更状态。 2： Action 可以包含任意异步操作。
const actions = {
    // 设置用户名
    setUserName({commit}, nickname) {
        commit('setUserName', nickname);
    },
    setInitFlag({commit}, flag) {
        commit('setInitFlag', flag);
    },
    setStartUsingDate({commit}, date) {
        commit('setStartUsingDate', date);
    },
    setSelectedIndex({commit}, index) {
        commit('setSelectedIndex', index);
    }
}

// mutations 更改 Vuex 的 store 中的状态的唯一方法是提交 mutation。这里只能包含同步方法。
const mutations = {
    setUserName(state, nickname) {
        state.username = nickname;
        // 写入到localStorage
        write(USERNAME, nickname)
    },
    setInitFlag(state, flag) {
        state.initFlag = flag;
        write(INIT_FLAG, flag);
    },
    setStartUsingDate(state, date) {
        state.startUsingDate = date;
        write(START_USING_DATE, date);
    },
    setSelectedIndex(state, index) {
        state.selectedIndex = index;
    },
}

// 导出以上的模块
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
```

一个模块(`module`)分别包含了`state`,`getters`,`actions`,`mutations`。

然后将该模块在`store/index.js`引入即可：

```js
import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import { createLogger } from 'vuex'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    app,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
```

然后我们需要将该状态管理的实例放入vue实例中，编辑`src/main.js`：

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 导入vuex实例
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import moment from 'moment/moment'

Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  router,
  // 导入到vue实例中
  store,
  render: h => h(App)
}).$mount('#app')
```

我们如何去使用呢？正如我们在SetUp.vue中那样：

```js
      // 点击开始
      completeSetup() {
          // 修改用户名
          this.$store.dispatch('app/setUserName', this.nickname);
          // 修改用户初始化状态
          this.$store.dispatch('app/setInitFlag', true);
          // 修改用户开始的时间
          this.$store.dispatch('app/setStartUsingDate', getTodayTime());
          // 跳转到主页面
          this.$router.replace({name: 'Home'});
      }
```

> 通过在根实例中注册 `store` 选项，该 store 实例会注入到根组件下的所有子组件中，且子组件能通过 `this.$store` 访问到。
>
> ```js
> this.$store.dispatch('app/setUserName', this.nickname);
> ```
>
> 以上的操作就会调用`actions`中的`setUserName`方法，另一个参数是传递到setUserName中的变量，而在`actions`中，第一个参数是与 store 实例具有相同方法和属性的 context 对象，而我们的`setUserName({commit}, nickname)`中使用的es6中的参数解构，在该函数的作用域中，commit就是context对象中的commit属性。
>
> ```js
> commit('setUserName', nickname);
> ```
>
> 在actions的方法中调用`commit('setUserName', nickname);`实际上是调用了`mutations`中的`setUserName`函数，在`mutations`的`setUserName`函数中，接受 state 作为第一个参数，额外的参数，即 mutation 的 **载荷（payload）**就是上述的`nickname`参数。state 就是上述的单一状态树，直接变更状态即可
>
> ```js
> state.username = nickname;
> ```
>
> 更改状态的唯一方法就是在`mutations`中。
>
> **单一状态树state**： 用一个对象就包含了全部的应用层级状态。至此，它便作为一个“唯一数据源”而存在。这也意味着每个应用将仅仅包含一个 store 实例。每当`store.state`变化的时候，都会重新求取计算数据，并且出发更新相关联的DOM。

> **this.$router**,通过在根实例中注册 `router` 选项，该 store 实例会注入到根组件下的所有子组件中，且子组件能通过 `this.$router` 访问到。
>
> 常用的方法：
>
> - ## `router.push(location, onComplete?, onAbort?)`
>
> 想要导航到不同的 URL，则使用 `router.push` 方法。这个方法会向 history 栈添加一个新的记录，所以，当用户点击浏览器后退按钮时，则回到之前的 URL。
>
> 在页面中，当你点击 `<router-link>` 时，这个方法会在内部调用，所以说，点击 `<router-link :to="...">` 等同于调用 `router.push(...)`。
>
> - ## `router.replace(location, onComplete?, onAbort?)`
>
> 跟 `router.push` 很像，唯一的不同就是，它不会向 history 添加新记录，而是跟它的方法名一样 —— 替换掉当前的 history 记录。
>
> - ## `router.go(n)`
>
> 这个方法的参数是一个整数，意思是在 history 记录中向前或者后退多少步，类似 `window.history.go(n)`。
>
> ---
>
> Vue Router 的导航方法 (`push`、 `replace`、 `go`) 在各类路由模式 (`history`、 `hash` 和 `abstract`) 下表现一致。
>
> 通过history api，我们丢掉了丑陋的#，但是它也有个问题：不怕前进，不怕后退，就怕刷新，f5，（如果后端没有准备的话）,因为刷新是实实在在地去请求服务器的,不玩虚的。 在hash模式下，前端路由修改的是#中的信息，而浏览器请求时是不带它玩的，所以没有问题.但是在history下，你可以自由的修改path，当刷新时，如果服务器中没有相应的响应或者资源，会分分钟刷出一个404来。
>
> hash兼容到ie8，history只能兼容到ie10;

以上就是开始页面涉及到的知识点。

### 首页

我们新建一个`Home.vue`，作为我们功能的主体页面。

```vue
<template>
  <el-container class="full-screen">
    <transition mode="out-in">
      <el-aside class="asider" :width="isCollapse ? '64px': '300px'">
        <app-left-control v-model="isCollapse"></app-left-control>
      </el-aside>
    </transition>
    <el-container>
      <app-right-control></app-right-control>
    </el-container>
    <transition name="fade" mode="out-in">
      <router-view></router-view>
    </transition>
  </el-container>
</template>

<style>
.full-screen {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
.asider {
  transition: all .2s;
}
.fade-enter-active, .fade-leave-active {
  transition: all .5s;
}
.fade-enter {
  opacity: 0;
  transform: translateX(450px);
}
.fade-leave-to {
  opacity: 0;
  transform: translateX(450px);
}
</style>

<script>
import AppLeftControl from '@/components/AppLeftControl';
import AppRightControl from '@/components/AppRightControl';
// @ is an alias to /src

export default {
  name: 'Home',
  components: {
    AppLeftControl,
    AppRightControl
  },
  data() {
    return {
      isCollapse: false,
      newListDialogVisible: false,
    }
  }
}
</script>
```

> 这里的Home页面的功能较少，主要逻辑会被抽到组件中去实现，该Home页面主要布局了左侧导航栏，以及右侧内容区域。当然还有关键的`<router-view></router-view>`，如果没有路由是Home的子路由，那么这个位置会为空白，我们后续将做上子路由，主要用于显示待办事项的详细内容。

> `<transition></transition>`包裹组件或者`<router-view></router-view>`会为其添加上切换动画，动画是css3的内容，主要是切换时的过渡效果。就不深入讲解了，详细请看vue的官方文档。

#### app-left-control组件

在这个组件中，我们用到了之前说过的`v-model`,主要用于我们可以拿到当前左侧导航的状态，便于改变组件父节点的宽度更新，当然还会有更好的办法，这里主要想用一下`v-model`;

```vue
<template>
  <div class="left-control" :class="{'collapse': isCollapse}">
      <div class="header-wrapper">
          <img src="../assets/imgs/default-avatar.png" alt="">
          <span class="username-text" v-if="!isCollapse">{{username}}</span>
          <div class="header-btn" v-if="!isCollapse">
              <i class="el-icon-setting" @click="toSettings"></i>
          </div>
      </div>
      <div class="list-wrapper">
          <el-menu :default-active="selectedIndex" class="el-menu-vertical-demo" @open="handleOpen" @close="handleClose" :collapse="isCollapse" @select="select">
                <el-menu-item index="1">
                    <i class="el-icon-s-home"></i>
                    <span slot="title">今天</span>
                </el-menu-item>
                <el-menu-item index="2">
                    <i class="el-icon-date"></i>
                    <span slot="title">待办清单</span>
                </el-menu-item>
                <template v-for="item in all">
                    <el-menu-item :index="item._id" :key="item._id">
                      <i class="el-icon-s-order"></i>
                      <span slot="title">{{item.title}}</span>
                    </el-menu-item>
                </template>
           </el-menu>
           <div class="add-list-btn-wrapper">
                    <template v-if="isCollapse">
                            <el-button icon='el-icon-plus'></el-button>
                    </template>
                    <template v-else>
                            <el-button icon='el-icon-plus' @click="newListDialogVisible = true">新列表</el-button>
                    </template>
           </div>
      </div>
      <div class="sider-trigger" :style="siderTriggerStyle" @click="toggle">
          <i :class="isCollapse ? 'el-icon-arrow-right' : 'el-icon-arrow-left'"></i>
      </div>
      <el-dialog title="添加新列表" :visible.sync="newListDialogVisible" width="30%" :modal-append-to-body='false'>
        <el-input placeholder="列表名称" v-model="newListTitle"></el-input>
        <span slot="footer" class="dialog-footer">
          <el-button @click="newListDialogVisible = false">取 消</el-button>
          <el-button type="primary" @click="insert" :disabled="newListTitle == ''">确 定</el-button>
        </span>
      </el-dialog>
  </div>
</template>

<script>
import {Logger} from '@/common/logger'
import { mapState } from 'vuex'

const logger = new Logger('appLeftControl');

export default {
  name: 'AppLeftControl',
  // 主要用于v-model
  model: {
      prop: 'isCollapse',
      event: 'change'
  },
  // 组件的参数列表，还有一系列复杂操作，验证数据类型，静态动态参数等等。由上级组件传入。
  props: {
      isCollapse: Boolean
  },
  data() {
      return {
          siderTriggerStyle: {
              width: '300px'
          },
          newListDialogVisible: false,
          newListTitle: '',
      }
  },
  // 侦听器，所侦听的变量发生变化时，方法就会执行，来响应数据的变化。
  watch: {
      // 如果`isCollapse`发生改变，这个函数就会执行
      isCollapse: function(newValue) {
          this.$emit('change', newValue);
          if (newValue) {
              this.siderTriggerStyle = {
                  width: '64px'
              }
          } else {
              this.siderTriggerStyle = {
                  width: '300px'
              }
          }
      },
      username: function() {
          console.log('值变了');
      }
  },
  methods: {
      created() {
          logger.debug('created');
      },
      toggle() {
          this.$emit('change', !this.isCollapse);
      },
      handleOpen() {

      },
      handleClose() {

      },
      toSettings() {
          this.$router.push({name: 'Settings'});
      },
      insert() {
          this.$store.dispatch('lists/insert', this.newListTitle);
          this.newListTitle = ''
          this.newListDialogVisible = false;
      },
      select(index) {
        this.$store.dispatch('app/setSelectedIndex', index);
      }
  },
  // 计算属性，计算属性是基于它们的响应式依赖进行缓存的。只在相关响应式依赖发生改变时它们才会重新求值。以下会有详细解读
  computed: {
      ...mapState('app', ['username', 'selectedIndex']),
      ...mapState('lists', ['all'])
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
.left-control {
    height: 100%;
    flex: 0 0 300px;
    max-width: 300px;
    min-width: 300px;
    width: 300px;
    position: relative;
    background-color: #fff;
    transition: all .2s;
}
.left-control.collapse {
    flex: 0 0 64px;
    max-width: 64px;
    min-width: 64px;
    width: 64px;
}
.left-control .header-wrapper {
    padding: 12px;
    display: flex;
    align-items: center;
    flex-wrap: nowrap;
    overflow: hidden;
    line-height: 0;
}
.left-control .list-wrapper {
    overflow-y: scroll;
    position: absolute;
    top: 60px;
    bottom: 48px;
    width: 100%;
    padding-bottom: 20px;
}
.left-control .header-wrapper img {
    width: 40px;
    height: 40px;
}
.left-control .header-wrapper .username-text {
    margin-left: 8px;
    flex: 1;
    line-height: 40px;
    font-size: 14px;
}
.left-control .header-wrapper .header-btn {
    padding: 12px;
    cursor: pointer;
    transition: all .2s linear;
    font-size: 20px;
}
.left-control .header-wrapper .header-btn:hover {
    transform: translate3d(0,-2px,0);
}
.left-control .list-wrapper .add-list-btn-wrapper {
    margin-top: 8px;
    text-align: center;
}
.el-menu-vertical-demo:not(.el-menu--collapse) {
    width: 300px;
  }
.left-control .list-wrapper::-webkit-scrollbar { width: 0 !important }
.left-control .sider-trigger {
    position: fixed;
    text-align: center;
    bottom: 0;
    cursor: pointer;
    height: 48px;
    line-height: 48px;
    z-index: 1;
    transition: all .2s;
    background-color: #efefef;
    font-size: 20px;
}
</style>
```

> 以上是`AppLeftControl`的整体代码。
>
> **`v-model`的使用**
>
> 可以看到
>
> ```js
> model: {
>       prop: 'isCollapse',
>       event: 'change'
>   }
> ```
>
> 利用名为 `isCollapse` 的 prop 和名为 `change` 的事件，来达到父组件与子组件变量一致的效果。
>
> ```js
> toggle() {
>   // 我们在isCollapse变更的时候触发了该事件
>   this.$emit('change', !this.isCollapse);
> }
> ```

> **计算属性**
>
> ```html
> <p>Reversed message: "{{ reversedMessage() }}"</p>
> ```
>
> ```js
> // 在组件中
> methods: {
>   reversedMessage: function () {
>     return this.message.split('').reverse().join('')
>   }
> }
> ```
>
> 我们可以将同一函数定义为一个方法而不是一个计算属性。两种方式的最终结果确实是完全相同的。然而，不同的是**计算属性是基于它们的响应式依赖进行缓存的**。只在相关响应式依赖发生改变时它们才会重新求值。这就意味着只要 `message` 还没有发生改变，多次访问 `reversedMessage` 计算属性会立即返回之前的计算结果，而不必再次执行函数。
>
> 我们为什么需要缓存？假设我们有一个性能开销比较大的计算属性 **A**，它需要遍历一个巨大的数组并做大量的计算。然后我们可能有其他的计算属性依赖于 **A**。如果没有缓存，我们将不可避免的多次执行 **A** 的 getter！如果你不希望有缓存，请用方法来替代。

> **mapState的使用**
>
> 在上述组件中，我们主要用的是用计算属性返回vuex的单一状态树state。
>
> ```js
> computed: {
>     // 使用对象展开运算符将此对象混入到外部对象中
>       ...mapState('app', ['username', 'selectedIndex']),
>       ...mapState('lists', ['all'])
>   }
> ```
>
> `mapState`是个比较好用的vuex辅助函数。
>
> 正常来说，我们要从store 实例中读取状态最简单的方法就是在[计算属性 (opens new window)](https://cn.vuejs.org/guide/computed.html)中返回某个状态：
>
> ```js
> computed: {
>     count () {
>       return store.state.count
>     }
>   }
> ```
>
> 每当 `store.state.count` 变化的时候, 都会重新求取计算属性，并且触发更新相关联的 DOM。
>
> 而`mapState`大大简化了操作，首先我们需要了解的是，我们在其中使用了es6中的对象展开运算符，使用对象展开运算符将此对象混入到外部对象中。第一个参数是我们的命名空间，我们当初module中用的就是`app`:
>
> ```js
> computed: {
>     // 使用对象展开运算符将此对象混入到外部对象中
>       ...mapState('app', ['username', 'selectedIndex'])
>   }
> ```
>
> 就等同于
>
> ```js
> computed: {
>     username() {
>         return store.state.username;
>     },
>     selectedIndex() {
>         return store.state.selectedIndex;
>     }
> }
> ```
>
> 我们可以直接在dom中使用
>
> ```html
> <span class="username-text" v-if="!isCollapse">{{username}}</span>
> ```

> **新建了另一个module保存待办事项的分类**
>
> lists.js
>
> ```js
> import {LISTS} from '@/common/local-storage-namespace';
> import {read, write} from '@/utils/local-storage';
> import {List} from '@/domain/entities';
> const state = () => ({
>   // 所有的列表
>   all: read(LISTS) || []
> })
> 
> // getters
> const getters = {
>   // 获取当前所在列表的titile
>   getSelectedTitle: (state) => (id) => {
>     const list = state.all.find(list => list._id === id);
>     return list ? list.title : '';
>   }
> }
> 
> // actions
> const actions = {
>   insert({commit}, title) {
>     commit('insert', new List(title));
>   }
> }
> 
> // mutations
> const mutations = {
>   // 插入数据
>   insert(state, list) {
>     state.all.push(list);
>     write(LISTS, state.all);
>   },
>   // 删除一条数据
>   destory(state, id) {
>     // 找到该条数据的索引
>     const index = state.all.findIndex((list) => list._id == id);
>     // 根据索引在数组中删除
>     state.all.splice(index, 1);
>     // 将数组持久化
>     write(LISTS, state.all);
>   },
>   // 更新列表的标题
>   updateTitle(state, {id, title}) {
>     // 找到该条数据的索引
>     const index = state.all.findIndex((list) => list._id == id);
>     // 更改
>     state.all[index].title =  title;
>     // 将数组持久化
>     write(LISTS, state.all);
>   }
> }
> 
> export default {
>   namespaced: true,
>   state,
>   getters,
>   actions,
>   mutations
> }
> ```
>
> 同样在`store/index.js`导入
>
> ```js
> import Vue from 'vue'
> import Vuex from 'vuex'
> import app from './modules/app'
> import lists from './modules/lists'
> import { createLogger } from 'vuex'
> 
> Vue.use(Vuex)
> 
> const debug = process.env.NODE_ENV !== 'production'
> 
> export default new Vuex.Store({
>   modules: {
>     app,
>     lists,
>   },
>   strict: debug,
>   plugins: debug ? [createLogger()] : []
> })
> ```

> **v-for的使用**
>
> ```html
> <template v-for="item in all">
>     <el-menu-item :index="item._id" :key="item._id">
>         <i class="el-icon-s-order"></i>
>         <span slot="title">{{item.title}}</span>
>     </el-menu-item>
> </template>
> ```
>
> 列表渲染：我们可以用 `v-for` 指令基于一个数组来渲染一个列表。`v-for` 指令需要使用 `item in items` 形式的特殊语法，其中 `items` 是源数据数组，而 `item` 则是被迭代的数组元素的**别名**。
>
> 在 `v-for` 块中，我们可以访问所有父作用域的 property。`v-for` 还支持一个可选的第二个参数，即当前项的索引。
>
> 为了给 Vue 一个提示，以便它能跟踪每个节点的身份，从而重用和重新排序现有元素，你需要为每项提供一个唯一 `key` attribute

> **v-if**的使用
>
> ```html
> <div class="header-wrapper">
>     <img src="../assets/imgs/default-avatar.png" alt="">
>     <span class="username-text" v-if="!isCollapse">{{username}}</span>
>     <div class="header-btn" v-if="!isCollapse">
>         <i class="el-icon-setting" @click="toSettings"></i>
>     </div>
> </div>
> ```
>
> 条件渲染：
>
> `v-if` 指令用于条件性地渲染一块内容。这块内容只会在指令的表达式返回 truthy 值的时候被渲染。即真值，指的是在[布尔值](https://developer.mozilla.org/zh-CN/docs/Glossary/Boolean)上下文中，转换后的值为真的值。所有值都是真值，除非它们被定义为 [假值](https://developer.mozilla.org/zh-CN/docs/Glossary/Falsy)（即除 `false`、`0`、`""`、`null`、`undefined` 和 `NaN` 以外皆为真值）。
>
> `v-if` 是“真正”的条件渲染，因为它会确保在切换过程中条件块内的事件监听器和子组件适当地被销毁和重建。
>
> `v-if` 也是**惰性的**：如果在初始渲染时条件为假，则什么也不做——直到条件第一次变为真时，才会开始渲染条件块。

以上页面就新增了这些知识点。

#### AppRightControl组件

```vue
<template>
  <el-container class="right-control ">
      <el-container class="header-wrapper">
        <app-header></app-header>
      </el-container>
      <el-container class="list-wrapper">
        <app-to-do></app-to-do>
      </el-container>
      <div class="quick-add-wrapper" :class="{'center': isEmpty}">
        <app-quick-add @add="add"></app-quick-add>
      </div>
  </el-container>
</template>

<script>
import AppHeader from '@/components/AppHeader'
import AppQuickAdd from '@/components/AppQuickAdd.vue'
import AppToDo from '@/components/AppToDo.vue'
import {Logger} from '@/common/logger'
import { mapGetters } from 'vuex'
const logger = new Logger('rightControl');
export default {
  name: 'AppRightControl',
  data() {
    return {
    }
  },
  props: {
  },
  components: {
    AppHeader,
    AppQuickAdd,
    AppToDo,
  },
  methods: {
    // 添加一个待办事项
    add(value) {
      logger.debug(value);
      this.$store.dispatch('todos/insert', value);
    }
  },
  computed: {
    ...mapGetters('todos', ['isEmpty'])
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
.right-control {
  position: relative;
  height: 100%;
  background-color: #f0f2f5;
  flex-direction: column;;
}
.header-wrapper {
    height: 180px;
    padding: 0;
    background-color: #fff;
    flex-basis: 180px;
    flex: none;
}
.right-control .quick-add-wrapper {
    position: absolute;
    left: 50%;
    bottom: 40px;
    width: 400px;
    -webkit-transform: translate3d(-50%,0,0);
    transform: translate3d(-50%,0,0);
    transition: .2s ease all;
}
.right-control .quick-add-wrapper.center {
  bottom: 50%;
}
</style>
```

> **vuex新增了一个module，用于保存待办事项，保存方式为数组**
>
> ```js
> import {TODOS} from '@/common/local-storage-namespace';
> import {read, write} from '@/utils/local-storage'
> import {Todo} from '@/domain/entities';
> import * as _ from 'lodash';
> import moment from 'moment/moment'
> const state = () => ({
>   // 全部的待办事项
>   all: read(TODOS) || []
> })
> 
> // getters
> const getters = {
>   // 根据条件获取待办事项
>   list(state, getters, rootState) {
>     // 获取当前选择的列表id
>     let listUUID = rootState.app.selectedIndex;
>     let result = [];
>     switch(listUUID) {
>       case '1':
>         result = getters.listTodayTodo;
>         break;
>       case '2':
>         result = getters.listTodo;
>         break;
>       default:
>         result = getters.listTodoByListUUID(listUUID);
>     }
>     return result.sort(todo => todo.completedFlag ? 1 : -1);
>   },
>   // 根据listUUID获取待办事项
>   listTodoByListUUID: (state) => (id) => {
>     return state.all.filter(todo => todo.listUUID === id);
>   },
>   // 获取今天的待办事项
>   listTodayTodo: (state) => {
>     return state.all.filter(todo => {
>       // 有计划时间
>       if (todo.planAt) {
>         return moment().isSameOrBefore(todo.planAt, 'day');
>       } else {
>         return false;
>       }
>     });
>   },
>   // 获取listUUID为空待办事项
>   listTodo:(state) => {
>     return state.all.filter(todo => !todo.listUUID);
>   },
>   // 获取当前列表的数量是否为空
>   isEmpty(state, getters) {
>     const list = getters.list;
>     return _.isEmpty(list);
>   },
>   // 根据id获取一个todo
>   getTodo: (state) => (id) => {
>     return state.all.find(todo => todo._id == id);
>   }
> }
> 
> // actions
> const actions = {
>   // 插入一个待办
>   insert({commit, rootState}, title) {
>     // 获取当前选择的列表 id
>     let selectedIndex = rootState.app.selectedIndex;
>     let listUUID = selectedIndex;
>     if (selectedIndex == '1' || selectedIndex == '2') {
>       listUUID = '';
>     }
>     // 获取当前的listUUID
>     const todo = new Todo({title, listUUID});
>     if (selectedIndex == '1') {
>       todo.planAt = new Date();
>     }
>     commit('insert', todo);
>   }
> }
> 
> // mutations
> const mutations = {
>   // 插入一个todo
>   insert(state, todo) {
>     state.all.unshift(todo);
>     write(TODOS, state.all);
>   },
>   // 删除待办
>   destory(state, id) {
>     // 找到该条数据的索引
>     const index = state.all.findIndex((todo) => todo._id == id);
>     // 根据索引在数组中删除
>     state.all.splice(index, 1);
>     // 将数组持久化
>     write(TODOS, state.all);
>   },
>   // 更新待办
>   update(state, updated) {
>     const todos = state.all;
>     // 找到该条数据的索引
>     const index = todos.findIndex((todo) => todo._id == updated._id);
>     // 更新
>     todos[index] = {...todos[index], ...updated};
>     state.all = [...todos];
>     // 将数组持久化
>     write(TODOS, state.all);
>   },
>   // 更新待办的完成状态
>   updateCompletedFlag(state, {id, flag}) {
>     const todos = state.all;
>     // 找到该条数据的索引
>     const index = todos.findIndex((todo) => todo._id == id);
>     // 更新
>     todos[index] = {...todos[index], ...{completedFlag: flag}};
>     state.all = [...todos];
>     // 将数组持久化
>     write(TODOS, state.all);
>   }
> }
> 
> export default {
>   namespaced: true,
>   state,
>   getters,
>   actions,
>   mutations
> }
> 
> ```

> **getters的使用**
>
> 有时候我们需要从 store 中的 state 中派生出一些状态，这就是`getters`的作用。
>
> 就像计算属性一样，getter 的返回值会根据它的依赖被缓存起来，且只有当它的依赖值发生了改变才会被重新计算。

> **mapGetters的使用**
>
> ```js
> computed: {
>     ...mapGetters('todos', ['isEmpty'])
> }
> ```
>
> `mapGetters`是个比较好用的vuex辅助函数。
>
> 以上的编写方式就是对：
>
> ```js
> computed: {
>     isEmpty() {
>         return this.$store.getters.isEmpty
>     }
> }
> ```
>
> 的简化，可能一两个属性看不出简化程度，但是需要大量属性的时候，就会发现大大简化了代码。

> **绑定HTML Class**
>
> ```html
> <div class="quick-add-wrapper" :class="{'center': isEmpty}">
>     <app-quick-add @add="add"></app-quick-add>
> </div>
> ```
>
> 以上 `:class="{'center': isEmpty}"`，如果`isEmpty`为真值，该dom就会加载上`center` class，用于动态修改样式特别好用。`v-bind:class` 指令也可以与普通的 class attribute 共存。

##### AppHeader组件

```vue
<template>
  <el-container class="header">
      <img class="background-img" src="../assets/imgs/logo.svg" alt="">
      <div class="list-title-wrapper">{{listTitle}}</div>
      <div class="action-btn-wrapper">
        <el-dropdown @command="handleCommand">
          <span class="el-dropdown-link">
            设置<i class="el-icon-arrow-down el-icon--right"></i>
          </span>
          <el-dropdown-menu slot="dropdown">
            <el-dropdown-item v-if="isList" command='rename'>重命名</el-dropdown-item>
            <el-dropdown-item v-if="isList" command='destory'>删除</el-dropdown-item>
          </el-dropdown-menu>
        </el-dropdown>
      </div>
      <el-dialog title="重命名" :visible.sync="renameDialogVisible" width="30%" :modal-append-to-body='false'>
        <el-input placeholder="列表名称" v-model="renameTitle"></el-input>
        <span slot="footer" class="dialog-footer">
          <el-button @click="renameDialogVisible = false">取 消</el-button>
          <el-button type="primary" @click="updateListName" :disabled="renameTitle == ''">确 定</el-button>
        </span>
      </el-dialog>
  </el-container>
</template>

<script>
import {Logger} from '@/common/logger'
import { mapGetters, mapState } from 'vuex'

const logger = new Logger('header');
export default {
  name: 'AppHeader',
  created() {
    logger.debug('created');
    const title = this.getSelectedTitle(this.selectedIndex);
    this.listTitle = title;
    if (this.selectedIndex!='1' && this.selectedIndex!='2') {
      this.isList = true;
    } else {
      this.isList = false;
    }
  },
  data() {
    return {
      listTitle: '',
      isList: false,
      renameDialogVisible: false,
      renameTitle: '',
    }
  },
  methods: {
    handleCommand(command) {
      switch(command) {
        case 'rename':
          this.rename();
          break;
        case 'destory':
          this.destory();
          break;
        default:
          logger.debug('not match command');
      }
    },
    // 重命名
    rename() {
      logger.debug('rename');
      this.renameTitle = this.listTitle;
      this.renameDialogVisible = true;
    },
    // 删除该列表
    destory() {
      logger.debug('destory');
      this.$store.commit('lists/destory', this.selectedIndex);
      this.$store.commit('app/setSelectedIndex', '1');
    },
    // 更新列表的标题
    updateListName() {
      this.$store.commit('lists/updateTitle', {id: this.selectedIndex, title: this.renameTitle})
      this.listTitle = this.renameTitle;
      this.renameDialogVisible = false;
    }
  },
  watch: {
    'selectedIndex'(value){
      const title = this.getSelectedTitle(value);
      this.listTitle = title;
      if (value!='1' && value!='2') {
        this.isList = true;
      } else {
        this.isList = false;
      }
    },
  },
  computed: {
    ...mapState('app', ['selectedIndex']),
    ...mapGetters('lists', ['getSelectedTitle'])
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
.header {
    position: relative;
    height: 100%;
    border-left: 1px solid #efefef;
}
.header .background-img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 120px;
  transform: translate3d(-50%,-50%,0);
}
.header .list-title-wrapper {
    position: absolute;
    left: 20px;
    bottom: 20px;
    font-size: 26px;
    line-height: 26px;
    font-weight: 500;
}
.header .action-btn-wrapper {
    position: absolute;
    bottom: 20px;
    right: 20px;
}
.el-dropdown-link {
  cursor: pointer;
  color: #409EFF;
}
.el-icon-arrow-down {
  font-size: 12px;
}
</style>

```

> **this.$store.commit**
>
> this.$store.commit('app/setSelectedIndex', '1');
>
> 这种形式是直接调用`mutations`中的方法，更新单一状态树。没有异步操作，建议直接绕过`actions`，直接调用`mutations`

##### AppQuickAdd组件

```vue
<template>
  <div class="quick-add">
      <el-input placeholder="想要做什么？" v-model="inputValue" @change='addTodo'></el-input>
  </div>
</template>

<script>
export default {
  name: 'HelloWorld',
  data() {
      return {
          inputValue: ''
      }
  },
  props: {
  },
  methods: {
      addTodo(value) {
          this.$emit('add', value);
          this.inputValue = ''
      },
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>
```

> 没啥子知识点，全是之前说过的

##### AppToDo组件

```vue
<template>
  <div class="todo">
      <el-table ref="multipleTable" :data="list" stripe :show-header="false" current-row-key='_id' @select="handleSelectionChange">
            <el-table-column
            type="selection"
            width="55">
            </el-table-column>
            <el-table-column
                prop="title"
                label="标题"
                >
            </el-table-column>
            <el-table-column
                label="计划时间"
                >
                <template slot-scope="scope">
                  <i class="el-icon-time"></i>
                  <span style="margin-left: 10px">{{ scope.row.planAt | dateFormat('YYYY年MM月DD日') }}</span>
                </template>
            </el-table-column>
            <el-table-column
                label="截至时间"
                >
                <template slot-scope="scope">
                  <i class="el-icon-date"></i>
                  <span style="margin-left: 10px">{{ scope.row.dueAt | dateFormat('YYYY年MM月DD日') }}</span>
                </template>
            </el-table-column>
            <el-table-column label="操作">
              <template slot-scope="scope">
                <el-button
                size="mini"
                @click="handleEdit(scope.row)">查看</el-button>
              </template>
            </el-table-column>

          </el-table>

  </div>
</template>

<script>
import {Logger} from '@/common/logger'
import { mapGetters, mapState } from 'vuex'
import * as _ from 'lodash'

const logger = new Logger('todo');
export default {
  name: 'AppToDo',
  props: {
  },
  mounted() {
    this.list.forEach((todo, index) => {
      this.$refs.multipleTable.toggleRowSelection(todo, todo.completedFlag);
    })
  },
  data() {
      return {
          todoList: []
      }
  },
  watch: {
    'selectedIndex': (value) => {
      logger.debug(value);
    },
    'list': function(todos) {
      logger.debug('watch list: %o', todos)
      this.$nextTick(() => {
        this.list.forEach((todo, index) => {
          this.$refs.multipleTable.toggleRowSelection(todo, todo.completedFlag);
        })
      })
    }
  },
  methods: {
    handleEdit(row) {
      this.$router.push({path: `home/detail/${row._id}`})
    },
    handleSelectionChange(val) {
      // 全部选中的
      const list = this.list.filter(todo => todo.completedFlag);
      let different = [];
      let flag;
      // 长度比较
      if (val.length > list.length) {
        // 代表新选中了元素
        different  = _.difference(val, list);
        flag = true;
      } else {
        // 代表有元素被取消选择
        different  = _.difference(list, val);
        flag = false;
      }
      // logger.debug(different);
      // 找到差异
      const updatedTodo = different[0];
      this.$store.commit('todos/updateCompletedFlag', {id: updatedTodo._id, flag})
    }
  },
  computed: {
    ...mapState('app', ['selectedIndex']),
    ...mapGetters('todos', ['list'])
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
.todo {
    flex: 1;
    padding: 10px;
}
</style>
```

> **`ref`**
>
> 期望一个字符串
>
> `ref` 被用来给元素或子组件注册引用信息。引用信息将会注册在父组件的 `$refs` 对象上。如果在普通的 DOM 元素上使用，引用指向的就是 DOM 元素；如果用在子组件上，引用就指向组件实例：
>
> ```html
> <!-- `this.$refs.p` 将会是该Dom节点 -->
> <p ref="p">hello</p>
> 
> <!-- `this.$refs.child` 将会是该组件实例 -->
> <child-component ref="child"></child-component>
> ```
>
> 关于 ref 注册时间的重要说明：因为 ref 本身是作为渲染结果被创建的，在初始渲染的时候你不能访问它们 - 它们还不存在！所以我会在`mounted`生命周期钩子中调用，实例被挂载后调用。

### 待办详情页面

```vue
<template>
  <div class="detail" @click="cancel()">
    <div class="container" @click.stop>
      <div class="align-right">
        <el-button type="success" icon="el-icon-check" circle @click="save"></el-button>
        <el-button type="danger" icon="el-icon-delete" circle @click="destory"></el-button>
      </div>
      <div class="align-left">
        <el-checkbox v-model="todo.completedFlag" @change="completedFlagChange">标记完成</el-checkbox>
      </div>
      <el-divider content-position="left">标题</el-divider>
      <el-input v-model="todo.title" :disabled="todo.completedFlag"></el-input>
      <el-divider content-position="left">日期与提醒</el-divider>
      <div class="align-left">
        <div>
          截止时间：
          <el-date-picker
            v-model="todo.dueAt"
            align="right"
            type="date"
            placeholder="选择日期"
            :disabled="todo.completedFlag"
            >
          </el-date-picker>
        </div>
        <div>
          计划时间：
          <el-date-picker
            class="margin-top-10"
            v-model="todo.planAt"
            type="datetime"
            placeholder="选择日期时间"
            align="right"
            :disabled="todo.completedFlag"
            >
          </el-date-picker>
        </div>
      </div>
      <div class="margin-top-20 align-left">
        计划提醒：
        <el-switch
          v-model="todo.notifyMe"
          active-color="#13ce66"
          inactive-color="#ff4949"
          :disabled="todo.completedFlag"
          >
        </el-switch>
      </div>
      <el-divider content-position="left">更多</el-divider>
      <div>
        <el-input
          type="textarea"
          :rows="2"
          placeholder="待办详情"
          v-model="todo.desc"
          :disabled="todo.completedFlag"
          >
        </el-input>
      </div>
    </div>
  </div>
</template>


<script>
import {Logger} from '@/common/logger'
import { mapGetters } from 'vuex';
// @ is an alias to /src

const logger = new Logger('Detail');

export default {
  name: 'Detail',
  components: {
  },
  created() {
    logger.debug('created');
    const params = this.$route.params;
    this._id = params.id;
    this.todo = this.getData(this._id);
  },
  data() {
    return {
      _id: '',
      todo: {
        _id: '',
        title: '',
        desc: '',
        dueAt: '',
        planAt: '',
        notifyMe: false,
        completedFlag: false,
      }
    }
  },
  methods: {
    cancel() {
      this.$router.push({name: 'Home'});
    },
    // 获取数据
    getData(id) {
      const todo = this.getTodo(id);
      return {
        _id: todo._id,
        title: todo.title,
        desc: todo.desc,
        dueAt: todo.dueAt,
        planAt: todo.planAt,
        notifyMe: todo.notifyMe,
        completedFlag: todo.completedFlag,
      }
    },
    // 保存
    save() {
      this.$store.commit('todos/update', this.todo);
      this.back();
    },
    // 删除该待办
    destory() {
      this.$store.commit('todos/destory', this._id);
      this.back();
    },
    // 返回
    back() {
      this.$router.go(-1);
    },
    completedFlagChange(value) {
      
    }
  },
  computed: {
    ...mapGetters('todos', ['getTodo'])
  }
}
</script>

<style>
.detail {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: -450px;
  background-color: rgba(0,0,0,.65);
  z-index: 1000;
}
.detail .container {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    box-sizing: border-box;
    width: 450px;
    padding: 20px;
    background-color: #fff;
    box-shadow: 0 0 12px rgb(0 0 0 / 15%);
}
.align-right {
  text-align: right;
}
.align-left {
  text-align: left;
}
.margin-top-10 {
  margin-top: 10px;
}
.margin-top-20 {
  margin-top: 20px;
}
</style>
```

> 之前我们说过，Detail页面要作为Home页面的子页面的，来填充`<router-view></rouer-view>`的。
>
> 所以我们`router/index.js`的结构就是
>
> ```js
> import Vue from 'vue'
> import VueRouter from 'vue-router'
> import SetUp from '../views/SetUp.vue'
> import Detail from '../views/Detail.vue'
> import Settings from '../views/Settings.vue'
> import store from '../store'
> // import {Logger} from '@/common/logger';
> 
> // const logger = new Logger('router');
> 
> 
> Vue.use(VueRouter)
> 
> const routes = [
>   {
>     path: '/',
>     redirect: { name: 'SetUp' },
>   },
>   {
>     path: '/setup',
>     name: 'SetUp',
>     component: SetUp,
>     meta: { setUp: true }
>   },
>   {
>     path: '/home',
>     name: 'Home',
>     // route level code-splitting
>     // this generates a separate chunk (about.[hash].js) for this route
>     // which is lazy-loaded when the route is visited.
>     component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue'),
>     meta: {requiredInit: true},
>     children: [
>       {
>       path: 'detail/:id',
>       name: 'Detail',
>       component: Detail
>       }
>     ]
>   },
>   {
>     path: '/settings',
>     name: 'Settings',
>     component: Settings,
>     meta: {requiredInit: true}
>   }
> ]
> 
> const router = new VueRouter({
>   mode: 'hash',
>   base: process.env.BASE_URL,
>   routes
> })
> 
> 
> router.beforeEach((to, from, next) => {
>   if (to.matched.some(record => record.meta.setUp)) {
>     // this route requires auth, check if logged in
>     // if not, redirect to login page.
>     const initFlag = store.getters['app/getInitFlag']
>     if (initFlag) {
>       next({
>         path: '/home'
>       })
>     } else {
>       next()
>     }
>   } else {
>     next() // 确保一定要调用 next()
>   }
> })
> 
> router.beforeEach((to, from, next) => {
>   if (to.matched.some(record => record.meta.requiredInit)) {
>     // this route requires auth, check if logged in
>     // if not, redirect to login page.
>     const initFlag = store.getters['app/getInitFlag']
>     if (!initFlag) {
>       next({
>         path: '/'
>       })
>     } else {
>       next()
>     }
>   } else {
>     next() // 确保一定要调用 next()
>   }
> })
> 
> export default router
> ```
>
> 其中，包含了id的传参，这是在url中带过去的，在Detail页面我们如何获取这个参数呢？
>
> ```js
> created() {
>     logger.debug('created');
>     const params = this.$route.params;
>     this._id = params.id;
>   }
> ```
>
> 在`this.$route.params`中就能拿到属性id，做进一步处理。

### 设置页面

```vue
<template>
  <div class="settings full-screen" >
      <div class="back-btn floating-btn" @click="back">
          <i class="el-icon-arrow-left"></i>
      </div>
      <div class="content">
          <div class="body">
              <img class="avatar" src="../assets/imgs/default-avatar.png" alt="">
              <el-divider>用户名</el-divider>
              <el-input placeholder="请键入用户名" v-model="username"></el-input>
              <el-divider>应用设置</el-divider>
          </div>
      </div>
  </div>
</template>


<script>
// @ is an alias to /src

export default {
  name: 'Settings',
  components: {
  },
  data() {
    return {
    }
  },
  methods: {
      // 返回上一页
      back() {
          this.$router.go(-1);
      }
  },
  computed: {
      username: {
          get () {
              return this.$store.state.app.username;
          },
          set (value) {
              this.$store.dispatch('app/setUserName', value);
          }
      }
  }
}
</script>

<style>
.settings {
    width: 100%;
    display: flex;
    justify-content: center;
    background-color: #fff;
}
.full-screen {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
.settings .back-btn {
    position: fixed;
    left: 20px;
    top: 20px;
    font-size: 24px;
}
.float-btn {
    padding: 12px;
    transition: all .2s linear;
}
.content {
    padding: 120px 0 0;
    width: 300px;
}
.content .body {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-bottom: 100px;
}
.content .body .avatar {
    width: 80px;
    height: 80px;
}

</style>
```

### 项目成型效果

![](https://assets.qipo.net/images/pro-demo-4.PNG)

![()](https://assets.qipo.net/images/pro-demo-1.PNG)

![](https://assets.qipo.net/images/pro-demo-4.PNG)

![](https://assets.qipo.net/images/pro-demo-2.PNG)

项目试用地址：https://todos.qipo.net/

项目代码地址：https://gitee.com/keyarea/vue-todo-demo
